<?php

error_reporting(0);

  include "dbconfig.php";

?>

<!DOCTYPE html>

<html>

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>JOBCAPS| Jobrequest</title>

    <!-- Tell the browser to be responsive to screen width -->

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.5 -->

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- DataTables -->

    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins

         folder instead of downloading all of them to reduce the load. -->

    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

  </head>

  <body class="hold-transition skin-blue sidebar-mini">

    <div class="wrapper">



      <?php require('mainheader.php'); ?>

     

     <?php require('sidebar.php'); ?>



      <!-- Content Wrapper. Contains page content -->

      <div class="content-wrapper">

        <!-- Content Header (Page header) -->

         <section class="content-header">

          <h1>

            <?php

$result=mysqli_query($con,"SELECT COUNT(*) AS total FROM jobs where status='0'");

$row= mysqli_fetch_assoc($result);

 ?>

                      

                        <span class="label label-danger">Total Jobs <?php echo $row['total']; ?>  </span>

            <!-- <small>New Employers </small> -->

          </h1>

         <!--  -->

        </section>



        <!-- Main content -->

        <section class="content">

          <div class="row">

            <div class="col-xs-12">

              



              <div class="box">

                <div class="box-header">

                  <h3 class="box-title">New Job Requests</h3>

                </div><!-- /.box-header -->

                 <?php

              //$query2 = mysqli_query($con,"SELECT * FROM registration ORDER BY name ASC") or die("insert query error!".mysqli_error());

              $ros=mysqli_query($con,"SELECT * FROM jobs where status='0'");

              if(!$ros)

              {

                  die('Could not get data: ' . mysql_error());

              }

            ?>

                <div class="box-body"> 

                  <table id="example1" class="table table-bordered table-striped">

                    <thead>

                      <tr>

                        <th>Company Name</th>

                         

                        <th>Job Title</th>

                        <th>Posted Date</th>

                       

                        <th>Location</th>

                        <th>Qualification</th>

                        <th>Last Date</th>

                        <th>Action</th>

                      </tr>

                    </thead>

                    <tbody>

                     <?php

                          while($row = mysqli_fetch_array($ros, MYSQL_ASSOC))

                    {



                   

              ?>

                      <tr>

                      

                        <td><?php echo $row['companyname'];?></td>

                       

                        <td><?php echo $row['jobtitle'];?></td>

                        <td><?php echo $row['posteddate'];?></td>

                        

                        <td><?php echo $row['location'];?></td>

                       

                        

                        <td><?php echo $row['qualification'];?></td>

                      

                        

                        <td><?php echo $row['lastdate'];?></td>

                        

                        

                        <td class="center">

           <a class="btn btn-success " href="jobsingle.php?id=<?php echo $row['id']; ?>&url=<?php echo $url ?>" >

                <i class="glyphicon glyphicon-zoom-in icon-white"></i>

                View

            </a>

            <a class="btn btn-info" href="changejob.php?id=<?php echo $row['id']; ?>&url=<?php echo $url ?>">

                <i class="glyphicon glyphicon-edit icon-white"></i>

                Approve

            </a>

            <a class="btn btn-danger" href="jobrqdelete.php?id=<?php echo $row['id']; ?>&url=<?php echo $url ?>">

                <i class="glyphicon glyphicon-trash icon-white"></i>

                Delete

            </a>

            

        </td>

                      </tr>

                      <?php

                              }

                            ?>

                    

                    </tbody>

                    

                  </table>

                </div><!-- /.box-body -->

              </div><!-- /.box -->

            </div><!-- /.col -->

          </div><!-- /.row -->

        </section><!-- /.content -->

      </div><!-- /.content-wrapper -->

     <?php require "footer.php" ?>

      <!-- Control Sidebar -->

       <?php include"controlsidebar.php"; ?><!-- /.control-sidebar -->

      <!-- Add the sidebar's background. This div must be placed

           immediately after the control sidebar -->

      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->



    <!-- jQuery 2.1.4 -->

    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>

    <!-- Bootstrap 3.3.5 -->

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- DataTables -->

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!-- SlimScroll -->

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <!-- FastClick -->

    <script src="plugins/fastclick/fastclick.min.js"></script>

    <!-- AdminLTE App -->

    <script src="dist/js/app.min.js"></script>

    <!-- AdminLTE for demo purposes -->

    <script src="dist/js/demo.js"></script>

    <!-- page script -->

    <script>

      $(function () {

        $("#example1").DataTable();

        $('#example2').DataTable({

          "paging": true,

          "lengthChange": false,

          "searching": false,

          "ordering": true,

          "info": true,

          "autoWidth": false

        });

      });

    </script>

  </body>

</html>

