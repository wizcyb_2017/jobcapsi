<?php  
error_reporting(0);
session_start();
include("dbconfig.php");
  
if(isset($_POST['em-submit']))  
{  
    $email=$_POST['email'];  
    $empassword=$_POST['empassword'];  

    $check_user="SELECT * FROM employer WHERE email='$email' AND empassword='$empassword' AND em_status='1'";  

    $run=mysqli_query($con,$check_user);  

    if(mysqli_num_rows($run))  
    {  
        echo "<script>window.open('employerprofile.php','_self')</script>";  

        $_SESSION['email']=$email;//here session is used and value of $user_email store in $_SESSION.  

    }  
    else  
    {  
      echo "<script>alert('Email or password is incorrect!')</script>";  
    }  
}  
if(isset($_POST['seek_submit']))  
{  
    $seekemail=$_POST['seekemail'];  
    $seekpassword=$_POST['seekpassword'];  
  
    $check_user1="SELECT * FROM seeker WHERE seekemail='$seekemail' AND seekpassword='$seekpassword' ";  
  
    $run1=mysqli_query($con,$check_user1);  
  
    if(mysqli_num_rows($run1))  
    {  
        echo "<script>window.open('seekerprofile.php','_self')</script>";  
  
        $_SESSION['seekemail']=$seekemail;//here session is used and value of $user_email store in $_SESSION.  
  
    }  
    else  
    {  
      echo "<script>alert('Email or password is incorrect!')</script>";  
    }  
} 


?>  
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Login and Register form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    
    <style type="text/css">
        body {
    padding-top: 90px;
}
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: #029f5b;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #59B2E0;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #59B2E6;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #53A3CD;
	border-color: #53A3CD;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

    </style>
   <!--  <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
    <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script> -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>
<?php include "header.php" ?>
<div class="intro-strip"> </div>
<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">JOB SEEKER</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">EMPLOYER</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form"  method="post" role="form" style="display: block;">
								<div style="margin-right:1px;margin-left:175px;"><h4>JOB SEEKER LOGIN</h4></div>
									<div class="form-group">
										<input type="email" name="seekemail" id="username" tabindex="1" class="form-control"  value="">
									</div>
									<div class="form-group">
										<input type="password" name="seekpassword" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												 <input type="submit" name="seek_submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In" >
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="forgotpassseek.php" tabindex="5" class="forgot-password">Job Seeker Forgot Password?</a>
												</div>
												<div class="text-center">
													Don't have an account? <a href="seekerregistration.php" tabindex="5" class="forgot-password">Register Now</a>
												</div>
											</div>
										</div>
									</div>
								</form>
								<form id="register-form" action="" method="post" role="form" style="display: none;">
								<div style="margin-right:1px;margin-left:175px;"><h4>EMPLOYER LOGIN</h4></div>
									<div class="form-group">
										<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="email" value="">
										
									</div>
									
									<div class="form-group">
										<input type="password" name="empassword" id="empassword" tabindex="2" class="form-control" placeholder="Password">
									
									</div>
						
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="em-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Log In">
												
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="forgotpassem.php" tabindex="5" class="forgot-password">Employer Forgot Password?</a>
												</div>
												<div class="text-center">
													Don't have an account? <a href="employerregistration.php" tabindex="5" class="forgot-password">Register Now</a>
												</div>
											</div>
										</div>
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});

</script>

</body>
</html>
